package com.example.nikolas.onactivityresult;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

public class LinearActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linear_layout);
    }
}
