package com.example.nikolas.onactivityresult;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class languageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_layout);
    }

    public void Lengted(View v)
    {
        Intent intent = new Intent();
        if(R.id.BtnRu==v.getId())
            intent.putExtra("len", "Русский");
        if(R.id.BtnEN==v.getId())
            intent.putExtra("len", "Английский");
        if(R.id.BtnUA==v.getId())
            intent.putExtra("len", "Украинский");

            setResult(RESULT_OK,intent);
            finish();



    }
}
