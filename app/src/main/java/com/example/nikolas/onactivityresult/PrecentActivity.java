package com.example.nikolas.onactivityresult;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class PrecentActivity  extends Activity {
    private EditText ET;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prcent_layout);
        ET = (EditText) findViewById(R.id.TxtName);
    }

    public void Precented(View v)
    {
        Intent intent1 = new Intent();
        intent1.putExtra("name", ET.getText().toString());
        setResult(RESULT_OK, intent1);
        finish();
    }
}
