package com.example.nikolas.onactivityresult;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.nikolas.onactivityresult.util.RequestCode;

public class MainActivity extends AppCompatActivity {

    TextView txtName, txtLen;
    Button BTN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtName = (TextView) findViewById(R.id.textView2);
        txtLen =  (TextView) findViewById(R.id.textView);
        BTN = (Button) findViewById(R.id.button9);
    }

    public void onShow(View v)
    {
        Intent intent;
        switch(v.getId())
        {
            case R.id.BtnP:
                intent= new Intent(this, PrecentActivity.class);
                startActivityForResult(intent, RequestCode.REQ_PRE);
                break;

            case R.id.BtnL:
                intent= new Intent(this, languageActivity.class);
                startActivityForResult(intent, RequestCode.REQ_LEN);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK)
        {
            if(requestCode==RequestCode.REQ_PRE)
            {
                String name = data.getStringExtra("name");
                txtName.setText("Рад знакомству "+name);
            }
            if(requestCode==RequestCode.REQ_LEN)
            {
                String len = data.getStringExtra("len");
                txtLen.setText("Ваш язык: "+len);
            }

        }else
        {
            Toast.makeText(this, "Ошибка", Toast.LENGTH_SHORT).show();
        }
    }

    public void OnClicB(View v)
    {
        AlertDialog.Builder b = new  AlertDialog.Builder(MainActivity.this);
        b.setTitle("Заголовок").setMessage("Информация").setCancelable(true).setIcon(R.drawable.img).
                setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"Вы не согласны", Toast.LENGTH_SHORT).show();
            }
        }).setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"Вы согласны", Toast.LENGTH_SHORT).show();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(getApplicationContext(),"Вы нажали кнопку отмены", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alert = b.create();
        alert.show();
    }

    public void linear(View v)
    {
        Intent intent = new Intent(this, LinearActivity.class);
        startActivity(intent);
    }

    public void MyGrid(View v)
    {
        startActivity(new Intent(this, GridActivity.class));
    }

    public void Implical(View v)
    {
        Intent intent = new Intent("NeActivity");
        startActivity(intent);
    }

    public  void OpenContact(View v)
    {
        Intent intent =new  Intent();
        intent.setComponent(new ComponentName("com.android.contacts", "com.android.contacts.DialtactsContactsEntryActivity"));
        startActivity(intent);
    }

    public  void Lis(View v)
    {
        startActivity(new Intent(this, lesson18Activity.class));
    }
}
